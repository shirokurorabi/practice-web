# doker削除
docker-compose down --rmi all

# doker build
docker-compose up --build

# doker install
docker-compose up -d

# dokcer 接続
 docker-compose exec exam bash
 
 
 #仕様(作成中)
    ログイン機能
    登録機能
    更新機能
    検索機能
    削除機能（検索結果から削除を行う）
    一括登録機能（アップロードしたcsvファイルから一括でユーザー登録を行う）
    api処理後ポップアップにて結果表示
    
    csvレコードイメージ
    'user_id1','user_password1','user_name1','A'
    'user_id2','user_password2','user_name2','B'
 
 
 #機能別進捗
 
 ・ログイン 100%
 
    状況・残タスク
        python - 全般                 完
        vue    - 暗号化               完
        vue    - api送信              完
        vue    - ログイン後画面処理   完
        vue    - パスワード隠し       完
 
 ・登録     100%
 
    状況・残タスク
        python - model側(DBアクセス)の機能 完
        vue    - バリデーション            完
           ※存在チェックとパスワード相違チェックのみの
             簡易バージョン
        vue    - ポップアップ              完
 
 ・更新  100%
 
    状況・残タスク
        python  - 全般        完
        vue     - api通信     完
        vue     - 画面全般    完
 
 ・削除  100%
 
     状況・残タスク
         python - 全般                            完
         vue    - 画面側の処理（検索機能に依存）  完
         vue    - api通信                         完
 
 ・検索  100%(スタイルのできはイマイチ)
 
     状況・残タスク
         python  - 全般         完
         vue     - api通信      完
         vue     - 検索結果表示 完
 
 ・ファイルアップロード  100%
    
    状況・残タスク
         python  - 全般              完
         vue     - api通信           完
         vue     - ファイル内容表示  完
         vue     - 画面処理          完
    
        