create table user_master
(
    user_id varchar(255) not null,
    password varchar(255) not null,
    user_name varchar(255) not null,
    admin varchar(255) not null,
    register_dt date null,
    update_time datetime null,
    primary key (user_id)
)
    charset=utf8;

create table user_master2
(
    seq_id int not null auto_increment,
    lon_del_kbn char(1) not null,
    user_id varchar(255) not null,
    password varchar(255) not null,
    user_name varchar(255) not null,
    admin varchar(255) not null,
    register_dt date null,
    update_time datetime null,
    primary key (seq_id, user_id)
)
    charset=utf8;