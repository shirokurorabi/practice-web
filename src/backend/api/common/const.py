# coding: utf-8

# 結果コード
RESULT_CODE_OK='001'
RESULT_CODE_ERR='002'
RESULT_CODE_ERR_SYSTEM='900'

# メッセージ
MESSAGE_OK_001='正常終了'
MESSAGE_ERR_001='すでに登録されているユーザーIDです'
MESSAGE_ERR_002='登録に失敗しました'
MESSAGE_ERR_003='古いパスワードを設定しています'
MESSAGE_ERR_004='IDまたはパスワードが一致しません'
MESSAGE_ERR_005='今までのパスワードが一致しません'
MESSAGE_ERR_006='CSVにすでに登録されているユーザーIDが存在します'
MESSAGE_ERR_SYSTEM='システムエラー'