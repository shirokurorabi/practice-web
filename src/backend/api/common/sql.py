# coding: utf-8
import sys

def search_sql_create(param):
    search_sql = 'SELECT user_id, user_name, admin FROM user_master'
    first_flg = True

    if param['user_id'] != '%%':
        if first_flg:
            first_flg = False
            search_sql += ' WHERE user_id LIKE %(user_id)s'
        else:
            search_sql += ' AND user_id LIKE %(user_id)s'

    if param['user_name'] != '%%':
        if first_flg:
            first_flg = False
            search_sql += ' WHERE user_name LIKE %(user_name)s'
        else:
            search_sql += ' AND user_name LIKE %(user_name)s'

    if param['admin'] != []:
        if first_flg:
            first_flg = False
            search_sql += ' WHERE admin IN %(admin)s'
        else:
            search_sql += ' AND admin IN %(admin)s'

    return search_sql


def add_user_list_sql(param):
    add_list_sql = 'INSERT INTO user_master ( user_id, password, user_name, admin, register_dt) VALUES '

    for i, x in enumerate(param):
        if i == 0:
            tmp_value = "( '%(user_id)s', '%(password)s', '%(user_name)s', '%(admin)s', '%(register_dt)s' )" % x
            add_list_sql += tmp_value
        else:
            tmp_value = ",( '%(user_id)s', '%(password)s', '%(user_name)s', '%(admin)s', '%(register_dt)s' )" % x
            add_list_sql += tmp_value


    return add_list_sql

auth_user = '''
    SELECT 
         user_id
        ,password
    FROM 
        user_master
    WHERE
        user_id = %(user_id)s
'''

sign_in_user = '''
    SELECT 
         user_id
        ,password
        ,user_name
        ,admin
    FROM 
        user_master
    WHERE
        user_id = %(user_id)s
'''

add_user_sql = '''
INSERT INTO user_master
  ( user_id
    ,password
    ,user_name
    ,admin
    ,register_dt
  ) 
  VALUES 
  (
    %(user_id)s,
    %(password)s,
    %(user_name)s,
    %(admin)s,
    %(register_dt)s
  )
'''

update_user_info_sql = '''
UPDATE
    user_master
SET
    user_name = %(user_name)s
    ,admin = %(admin)s
    ,update_time = %(update_time)s
WHERE
    user_id = %(user_id)s
'''

update_password_sql = '''
UPDATE
    user_master
SET
    password = %(password)s
WHERE
    user_id = %(user_id)s
'''

del_user_sql = '''
DELETE FROM
    user_master
WHERE
    user_id = %(user_id)s
'''

get_user_list_sql = '''
SELECT 
    user_id
    ,user_name
    ,admin
FROM
    user_master
WHERE
    user_id IN %(user_id_list)s
'''
