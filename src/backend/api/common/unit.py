import sys
import os


def format_return_param(code='001', message='', data=None):
    res = {
        'result_cd': code,
        'message': message,
        'data': data
    }

    return res