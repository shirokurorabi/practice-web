import io
import sys
import json
import datetime
import api.common.unit as unit
import api.common.const as constant
import api.model.user as User
import api.common.sql as format


def user_sign_in(user_id, password):
    try:
        param = {
            'user_id': user_id,
            'password': password
        }

        user_auth_info = User.get_user_db(param)
        if user_auth_info is not None:
            if param['password'] == user_auth_info['password']:
                result = unit.format_return_param(
                    constant.RESULT_CODE_OK,
                    constant.MESSAGE_OK_001,
                    user_auth_info
                )
            else:
                result = unit.format_return_param(
                    constant.RESULT_CODE_ERR,
                    constant.MESSAGE_ERR_004
                )
        else:
            result = unit.format_return_param(
                constant.RESULT_CODE_ERR,
                constant.MESSAGE_ERR_004
            )

    except Exception as e:
        result = unit.format_return_param(
            constant.RESULT_CODE_ERR_SYSTEM,
            constant.MESSAGE_ERR_SYSTEM
        )

    return result


def user_add(user_id, password, user_name, admin):
    try:
        to_day = datetime.datetime.today()
        param = {
            'user_id': user_id,
            'password': password,
            'user_name': user_name,
            'admin': admin,
            'register_dt': to_day.strftime("%Y-%m-%d"),
        }

        user_info = User.get_user_db(param)

        if not user_info:
            # すでに登録されていなければ登録処理を行う
            res = User.user_add_db(param)
            if res is None:
                result = unit.format_return_param(
                    constant.RESULT_CODE_OK,
                    constant.MESSAGE_OK_001
                )
            else:
                result = unit.format_return_param(
                    constant.RESULT_CODE_ERR_SYSTEM,
                    constant.MESSAGE_ERR_SYSTEM
                )
        else:
            # すでに登録されていれば論理エラー
            result = unit.format_return_param(
                constant.RESULT_CODE_ERR,
                constant.MESSAGE_ERR_001
            )

    except Exception as e:
        result = unit.format_return_param(
            constant.RESULT_CODE_ERR_SYSTEM,
            constant.MESSAGE_ERR_SYSTEM
        )

    return result


def user_update(user_id, user_name, admin):
    try:
        to_date = datetime.datetime.today()
        param = {
            'user_id': user_id,
            'user_name': user_name,
            'admin': admin,
            'update_time': to_date.strftime("%Y-%m-%d %H:%M:%S")
        }

        res = User.user_update_db(param)

        if res is None:
            result = unit.format_return_param(
                constant.RESULT_CODE_OK,
                constant.MESSAGE_OK_001
            )
        else:
            result = unit.format_return_param(
                constant.RESULT_CODE_ERR_SYSTEM,
                constant.MESSAGE_ERR_SYSTEM
            )

    except Exception as e:
        result = unit.format_return_param(
            constant.RESULT_CODE_ERR_SYSTEM,
            constant.MESSAGE_ERR_SYSTEM
        )

    return result


def password_update(user_id, password_old, password_new):
    try:
        param_old = {
            'user_id': user_id,
            'password': password_old
        }
        param_new = {
            'user_id': user_id,
            'password': password_new
        }

        res = User.get_user_db(param_old)

        if res['password'] == param_old['password']:
            if res['password'] != param_new['password']:
                res_password = User.password_update_db(param_new)
                if res_password is None:
                    result = unit.format_return_param(
                        constant.RESULT_CODE_OK,
                        constant.MESSAGE_OK_001
                    )
                else:
                    result = unit.format_return_param(
                        constant.RESULT_CODE_ERR_SYSTEM,
                        constant.MESSAGE_ERR_SYSTEM
                    )
            else:
                result = unit.format_return_param(
                    constant.RESULT_CODE_ERR,
                    constant.MESSAGE_ERR_003
                )
        else:
            result = unit.format_return_param(
                constant.RESULT_CODE_ERR,
                constant.MESSAGE_ERR_005
            )

    except Exception as e:
        result = unit.format_return_param(
            constant.RESULT_CODE_ERR_SYSTEM,
            constant.MESSAGE_ERR_SYSTEM
        )

    return result


def user_search(user_id, user_name, admin):
    param = {
        'user_id': '%' + user_id + '%',
        'user_name': '%' + user_name + '%',
        'admin': admin
    }

    res = User.search_user_db(param)
    if 'err' not in res:
        result = unit.format_return_param(
            constant.RESULT_CODE_OK,
            constant.MESSAGE_OK_001,
            res
        )
    else:
        result = unit.format_return_param(
            constant.RESULT_CODE_ERR_SYSTEM,
            constant.MESSAGE_ERR_SYSTEM
        )

    return result


def user_delete(user_id):
    try:
        res = User.user_delete_db({'user_id': user_id})
        if res is None:
            result = unit.format_return_param(
                constant.RESULT_CODE_OK,
                constant.MESSAGE_OK_001
            )
        else:
            result = unit.format_return_param(
                constant.RESULT_CODE_ERR_SYSTEM,
                constant.MESSAGE_ERR_SYSTEM
            )

    except Exception as e:
        result = unit.format_return_param(
            constant.RESULT_CODE_ERR_SYSTEM,
            constant.MESSAGE_ERR_SYSTEM
        )

    return result


def user_csv_add(user_list):
    try:
        to_day = datetime.datetime.today()
        param_list = []
        param_user_list = []
        for i in user_list:
            tmp_param = {
                'user_id': i['user_id'],
                'password': i['password'],
                'user_name': i['user_name'],
                'admin': i['admin'],
                'register_dt': to_day.strftime("%Y-%m-%d"),
            }
            param_list.append(tmp_param)
            param_user_list.append(tmp_param['user_id'])

        user_info_list = User.get_user_list_db({'user_id_list': param_user_list})

        if not user_info_list:
            # すでに登録されていなければ登録処理を行う
            res = User.user_list_add_db(param_list)
            if res is None:
                result = unit.format_return_param(
                    constant.RESULT_CODE_OK,
                    constant.MESSAGE_OK_001
                )
            else:
                result = unit.format_return_param(
                    constant.RESULT_CODE_ERR_SYSTEM,
                    constant.MESSAGE_ERR_SYSTEM
                )
        else:
            # すでに登録されていれば論理エラー
            result = unit.format_return_param(
                constant.RESULT_CODE_ERR,
                constant.MESSAGE_ERR_006
            )

    except Exception as e:
        result = unit.format_return_param(
            constant.RESULT_CODE_ERR_SYSTEM,
            constant.MESSAGE_ERR_SYSTEM
        )


    return result