import io
import sys
import json
import pymysql
from api.util.setting import Setting
import api.common.sql as format


class User(object):
    def __init__(self, user_id=None, password=None, user_name=None, admin=None):
        self.user_id = user_id,
        self.password = password,
        self.user_name = user_name,
        self.admin = admin


    def to_dict(self):
        res = {
            "user_id": self.user_id,
            "password": self.password,
            "user_name": self.user_name,
            "admin": self.admin
        }

        return res


def get_user_db(param):
    conn = pymysql.connect(**Setting().db_init)
    cur = conn.cursor(pymysql.cursors.DictCursor)
    try:
        cur.execute(format.sign_in_user, param)
        result = cur.fetchone()
    except Exception as e:
        conn.rollback()
        return { 'err': e }

    finally:
        cur.close()
        conn.close()

    return result

def user_add_db(param):
    conn = pymysql.connect(**Setting().db_init)
    cur = conn.cursor(pymysql.cursors.DictCursor)
    try:
        cur.execute(format.add_user_sql, param)
        conn.commit()

    except Exception as e:
        conn.rollback()
        return { 'err': e }

    finally:
        cur.close()
        conn.close()

    return


def user_update_db(param):
    conn = pymysql.connect(**Setting().db_init)
    cur = conn.cursor(pymysql.cursors.DictCursor)
    try:
        cur.execute(format.update_user_info_sql, param)
        conn.commit()

    except Exception as e:
        conn.rollback()
        return { 'err': e }

    finally:
        cur.close()
        conn.close()

    return


def password_update_db(param):
    conn = pymysql.connect(**Setting().db_init)
    cur = conn.cursor(pymysql.cursors.DictCursor)
    try:
        cur.execute(format.update_password_sql, param)
        conn.commit()

    except Exception as e:
        conn.rollback()
        return { 'err': e }

    finally:
        cur.close()
        conn.close()

    return


def search_user_db(param):
    conn = pymysql.connect(**Setting().db_init)
    cur = conn.cursor(pymysql.cursors.DictCursor)
    try:
        sql = format.search_sql_create(param)
        cur.execute(sql, param)
        result = cur.fetchall()

    except Exception as e:
        print('model error', file=sys.stderr)
        print(e, file=sys.stderr)
        conn.rollback()
        return {'err': e}

    finally:
        cur.close()
        conn.close()

    return result


def user_delete_db(param):
    conn = pymysql.connect(**Setting().db_init)
    cur = conn.cursor(pymysql.cursors.DictCursor)
    try:
        cur.execute(format.del_user_sql, param)
        conn.commit()

    except Exception as e:
        conn.rollback()
        return { 'err': e }

    finally:
        cur.close()
        conn.close()

    return


def get_user_list_db(param):
    conn = pymysql.connect(**Setting().db_init)
    cur = conn.cursor(pymysql.cursors.DictCursor)
    try:
        cur.execute(format.get_user_list_sql, param)
        result = cur.fetchall()
    except Exception as e:
        conn.rollback()
        return { 'err': e }

    finally:
        cur.close()
        conn.close()

    return result


def user_list_add_db(param):
    conn = pymysql.connect(**Setting().db_init)
    cur = conn.cursor(pymysql.cursors.DictCursor)
    try:
        sql = format.add_user_list_sql(param)
        cur.execute(sql)
        conn.commit()

    except Exception as e:
        conn.rollback()
        return { 'err': e }

    finally:
        cur.close()
        conn.close()

    return
