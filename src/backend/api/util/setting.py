# coding: utf-8
import os
import sys


class Setting(object):
    def __init__(self):
        self.db_init = {
            'host': 'exam_db',
            'user': 'root',
            'password': 'root',
            'db': 'exam_db',
            'charset': 'utf8',
            'port': 3306
        }