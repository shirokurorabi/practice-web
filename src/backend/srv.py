import io
import sys
import json
from flask_cors import CORS
from flask import Flask, jsonify, request, redirect, url_for, Response, make_response, abort
import api.controller.user as user
sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='utf-8')

app = Flask(__name__)
CORS(app)


def return_param(result):

    res = Response()

    if result is not None:
        if 'err' in result.keys():
            res.status_code = 400
        else:
            res.status_code = 200

        res = jsonify(result_cd=result['result_cd'], message=result['message'], data=result['data'])
    else:
        res.status_code = 500
        res.message = 'システムエラー'
        res.result_code = 900

    res.headers['Content-Type'] = 'application/json;charset=UTF-8'
    res.headers['Access-Control-Allow-Origin'] = '*'
    res.headers['Access-Control-Allow-Methods'] = 'POST'
    res.headers['Access-Control-Allow-Headers'] = '*'

    return res


@app.route('/health', methods=['POST'])
def checkHealth():
    return 'Health 3010 exam Check OK!'


@app.route('/health2', methods=['POST'])
def checkHealth2():
    res = request.json['hea']
    return return_param(res)


@app.route('/api/sign_in', methods=['POST'])
def sign_in():
    user_id = request.json['user_id']
    password = request.json['password']
    res = user.user_sign_in(user_id, password)
    return return_param(res)


@app.route('/api/user/user_add', methods=['POST'])
def user_add():
    user_id = request.json['user_id']
    password = request.json['password']
    user_name = request.json['user_name']
    admin = request.json['admin']
    res = user.user_add(user_id, password, user_name, admin)
    return return_param(res)


@app.route('/api/user/user_update', methods=['POST'])
def user_update():
    user_id = request.json['user_id']
    user_name = request.json['user_name']
    admin = request.json['admin']
    res = user.user_update(user_id, user_name, admin)
    return return_param(res)


@app.route('/api/user/password_update', methods=['POST'])
def password_update():
    user_id = request.json['user_id']
    password_old = request.json['password_old']
    password_new = request.json['password_new']
    res = user.password_update(user_id, password_old, password_new)
    return return_param(res)


@app.route('/api/user/user_search',methods=['POST'])
def user_search():
    user_id = request.json['user_id']
    user_name = request.json['user_name']
    admin = request.json['admin']
    res = user.user_search(user_id, user_name, admin)
    return return_param(res)

@app.route('/api/user/user_delete', methods=['POST'])
def user_del():
    user_id = request.json['user_id']
    res = user.user_delete(user_id)
    return return_param(res)


@app.route('/api/user/user_csv_add', methods=['POST'])
def user_csv_add():
    user_list = request.json['user_list']
    res = user.user_csv_add(user_list)
    return return_param(res)


if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0', port=3010)