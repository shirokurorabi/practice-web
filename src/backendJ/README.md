#ビルド
gradle build

#コンパイル(bootRun時はホットデプロイ可)
gradle classes

#サーバー起動
gradle bootRun
    
 #機能(java springによるバックエンド)
    ログイン機能
    登録機能
    更新機能
    検索機能
    削除機能
    一括登録機能（アップロードしたcsvファイルから一括でユーザー登録を行う）
    
    csvレコードイメージ
    'user_id1','user_password1','user_name1','A'
    'user_id2','user_password2','user_name2','B'
    
    user_master2 - 論理削除区分を追加
    0:有効 1:削除
 
 
 #機能別進捗
 
 ・ログイン 100%
 
    状況・残タスク
        特になし

 
 ・登録     100%
 
    状況・残タスク
        api jsonデータ受け取り    -  完
        api ステータス返却        -  完
        DB  登録処理              -  完
        (例外処理・statuscode設定は要改善)     
        

 
 ・更新  100%
 
    状況・残タスク
        api送信
        ユーザー更新     - 完
        パスワード更新   - 完


 ・削除  100%
 
     状況・残タスク
        DBレコード削除  - 完
        api送信         - 完

 
 ・検索  100%
 
     状況・残タスク
        api送信        -  完
        検索リスト返却  -  完
        条件指定        -  完
 
 
 ・ファイルアップロード  100%
    
    状況・残タスク
        INSERT条件指定   - 完
        api送信          - 完


 ★気になる・・・
 
    改善を考えたい（改定が多いのでできたらやる）
        Responseを作成する箇所       - 基本serviceにて作成(例外時はcontrollerで作成)
        更新系DAOの戻り値の見直し     - 戻り値をintで返却