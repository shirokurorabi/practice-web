package com.example.examJ;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;


@Configuration
@ComponentScan("com.example.examJ")
public class AppConfig {
    // @ConfigurationをつけることでDIコンテナの設定ファイルであるとします
    // @ComponentScanでスキャンするパッケージを指定します
}
