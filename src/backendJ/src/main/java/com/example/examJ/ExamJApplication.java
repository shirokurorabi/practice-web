package com.example.examJ;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.CrossOrigin;


@SpringBootApplication
@CrossOrigin
public class ExamJApplication {

    // メイン処理 (bootRun)
    public static void main(String[] args) {
        SpringApplication.run(ExamJApplication.class, args);
    }

}