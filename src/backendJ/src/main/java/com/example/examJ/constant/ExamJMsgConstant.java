package com.example.examJ.constant;

public class ExamJMsgConstant {
    // メッセージ コンスタント  パッとわかりやすいような定数にしました
    public static final String MESSAGE_OK_001 = "正常終了";
    public static final String MESSAGE_ERR_ID_ALREDY = "すでに登録されているユーザーIDです";
    public static final String MESSAGE_ERR_ADD_FAILD = "登録に失敗しました";
    public static final String MESSAGE_ERR_OLDPASS_SET = "古いパスワードを設定しています";
    public static final String MESSAGE_ERR_IDPASS_UNMATCH = "IDまたはパスワードが一致しません";
    public static final String MESSAGE_ERR_OLDPASS_UNMATCH = "今までのパスワードが一致しません";
    public static final String MESSAGE_ERR_CSV_ALREADY = "CSVにすでに登録されているユーザーIDが存在します";
    public static final String MESSAGE_ERR_SYSTEM = "システムエラー";
}
