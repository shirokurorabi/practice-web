package com.example.examJ.constant;

public class  ExamJResultConstant {
    // リザルトコード コンスタント
    public static final String RESULT_CODE_OK = "001";
    public static final String RESULT_CODE_ERR = "002";
    public static final String RESULT_CODE_ERR_SYSTEM = "900";
}
