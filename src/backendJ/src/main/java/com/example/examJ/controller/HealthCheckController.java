package com.example.examJ.controller;

import org.springframework.web.bind.annotation.*;
import com.example.examJ.repository.dto.HealthCheck;

@RestController
@RequestMapping(value = "health")
public class HealthCheckController {

    // ヘルスチェック
    @CrossOrigin
    @PostMapping(path = "check")
    public HealthCheck Index() {
        HealthCheck healthCheck = new HealthCheck("ttt", "ccc");

        return healthCheck;
    }

}
