package com.example.examJ.controller;

import com.example.examJ.constant.ExamJMsgConstant;
import com.example.examJ.constant.ExamJResultConstant;
import com.example.examJ.repository.dto.RequestListAddParamDto;
import com.example.examJ.repository.dto.ResponseDto;
import com.example.examJ.service.UserCsvAddService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "api")
public class UserCsvAddController {

    @Autowired
    UserCsvAddService userCsvAddService;

    @CrossOrigin
    @PostMapping(path = "user/user_csv_add")
    public ResponseDto UserCsvAddCon (@RequestBody RequestListAddParamDto RequestListParam) {

        ResponseDto result;
        try {
            result = userCsvAddService.AddCsvUser(RequestListParam);
        } catch(Exception e) {
            result = new ResponseDto(
                    ExamJResultConstant.RESULT_CODE_ERR_SYSTEM,
                    ExamJMsgConstant.MESSAGE_ERR_SYSTEM
            );
        }


        return result;
    }
}
