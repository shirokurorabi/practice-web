package com.example.examJ.controller;

import com.example.examJ.constant.ExamJMsgConstant;
import com.example.examJ.constant.ExamJResultConstant;
import com.example.examJ.repository.dto.ResponseDto;
import com.example.examJ.repository.dto.RequestUserMasterDto;
import com.example.examJ.service.UserDeleteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "api")
public class UserDeleteController {

    @Autowired
    UserDeleteService userDeleteService;

    // ユーザー登録コントローラー
    // @RequestBodyでフロントエンドからのbody部データを受け取る
    @CrossOrigin
    @PostMapping(path = "user/user_delete")
    public ResponseDto UserDelete (@RequestBody RequestUserMasterDto Param) {

        ResponseDto result;

        try {
            result = userDeleteService.DelUser(Param);
        } catch(Exception e) {
            result = new ResponseDto(
                    ExamJResultConstant.RESULT_CODE_ERR_SYSTEM,
                    ExamJMsgConstant.MESSAGE_ERR_SYSTEM
            );
        }

        return result;
    }
}
