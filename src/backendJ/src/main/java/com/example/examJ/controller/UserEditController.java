package com.example.examJ.controller;

import com.example.examJ.constant.ExamJMsgConstant;
import com.example.examJ.constant.ExamJResultConstant;
import com.example.examJ.repository.dto.ResponseDto;
import com.example.examJ.repository.dto.RequestUserMasterDto;
import com.example.examJ.service.UserEditService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping(value = "api")
public class UserEditController {

    @Autowired
    UserEditService userEditService;

    // ユーザー編集用コントローラー
    @CrossOrigin
    @PostMapping(path = "user/user_update")
    public ResponseDto UserEditCon (@RequestBody RequestUserMasterDto RequestParam) {

        ResponseDto result;
        try {
            result = userEditService.EditUser(RequestParam);
        } catch(Exception e) {
            result = new ResponseDto(
                    ExamJResultConstant.RESULT_CODE_ERR_SYSTEM,
                    ExamJMsgConstant.MESSAGE_ERR_SYSTEM
            );
        }

        return result;
    }
}
