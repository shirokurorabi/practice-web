package com.example.examJ.controller;


import com.example.examJ.constant.ExamJMsgConstant;
import com.example.examJ.constant.ExamJResultConstant;
import com.example.examJ.repository.dto.RequestPassEditDto;
import com.example.examJ.repository.dto.ResponseDto;
import com.example.examJ.service.UserPassEditService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "api")
public class UserPassEditController {

    @Autowired
    UserPassEditService userPassEditService;

    @CrossOrigin
    @PostMapping(path = "user/password_update")
    public ResponseDto PassEditCon(@RequestBody RequestPassEditDto RequestParam) {

        ResponseDto result;
        try {
            result = userPassEditService.PassEdit(RequestParam);
        } catch(Exception e) {
            result = new ResponseDto(
                    ExamJResultConstant.RESULT_CODE_ERR_SYSTEM,
                    ExamJMsgConstant.MESSAGE_ERR_SYSTEM
            );
        }

        return result;
    }
}
