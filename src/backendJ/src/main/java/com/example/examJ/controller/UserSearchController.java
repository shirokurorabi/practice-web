package com.example.examJ.controller;

import com.example.examJ.constant.ExamJMsgConstant;
import com.example.examJ.constant.ExamJResultConstant;
import com.example.examJ.repository.dto.ResponseListDto;
import com.example.examJ.repository.dto.RequestSearchParamDto;
import org.springframework.beans.factory.annotation.Autowired;
import com.example.examJ.service.UserSearchService;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping(value = "api")
public class UserSearchController {

    // サービスインスタンスを自動注入する
    @Autowired
    UserSearchService userSearchService;

    @CrossOrigin
    @PostMapping(path = "user/user_search")
    public ResponseListDto UserSearch(@RequestBody RequestSearchParamDto Param){

        ResponseListDto result;
        try {
            result = userSearchService.SearchUser(Param);
        } catch(Exception e) {
            result = new ResponseListDto(
                    ExamJResultConstant.RESULT_CODE_ERR_SYSTEM,
                    ExamJMsgConstant.MESSAGE_ERR_SYSTEM
            );
        }

        return result;
    }

}
