package com.example.examJ.repository.dao;

import com.example.examJ.repository.dto.AddListParamDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import java.util.Map;

@Repository
public class AddListParamDao {

    @Autowired
    JdbcTemplate jdbcTemplate;

    // 本当はResponseDtoはserviceにて作成したい
    public int AddList (AddListParamDto Param) {

        String sql = "INSERT INTO user_master2 (lon_del_kbn, user_id, password, user_name, admin, register_dt) VALUES ";

        StringBuilder InserRec = new StringBuilder();
        for (Map<String, Object> rec : Param.getUserList()) {
            InserRec.append("('0',");
            for (Map.Entry<String, Object> map : rec.entrySet()) {
                String param = "'" + map.getValue() + "'";
                InserRec.append(param).append(',');
            }
            InserRec.append("'").append(Param.getRegisterDt()).append("'");
            InserRec.append("),");
        }
        InserRec.setLength(InserRec.length() - 1);
        sql +=  InserRec;

        // 戻り値として更新件数を返却
        return jdbcTemplate.update(sql);
    }
}
