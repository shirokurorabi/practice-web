package com.example.examJ.repository.dao;

import com.example.examJ.repository.dto.UserMasterDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;


@Repository
public class AddParamDao {

    // データベースにアクセスするためのモジュール
    @Autowired
    private JdbcTemplate jdbcTemplate;


    // DTOの値を設定することでINSERTを行う
    public int Add(UserMasterDto AddParam) throws Exception {
        return jdbcTemplate.update(
                "INSERT INTO " +
                "user_master2" +
                "(lon_del_kbn," +
                "user_id," +
                "password," +
                "user_name," +
                "admin," +
                "register_dt) " +
                "VALUES " +
                "(?, ?, ?, ?, ?, ?)",
                AddParam.getLonDelKbn(),
                AddParam.getUserId(),
                AddParam.getPassword(),
                AddParam.getUserName(),
                AddParam.getAdmin(),
                AddParam.getRegisterDt()
        );
    }
}
