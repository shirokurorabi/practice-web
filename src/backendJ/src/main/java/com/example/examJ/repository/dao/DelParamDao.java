package com.example.examJ.repository.dao;

import com.example.examJ.constant.ExamJLondelConstant;
import com.example.examJ.repository.dto.DeleteParamDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;


@Repository
public class DelParamDao {

    @Autowired
    JdbcTemplate jdbcTemplate;

    public int DelRec(DeleteParamDto DeleteParam) throws Exception {
        // 論理削除区分をONにして削除を行う
            return jdbcTemplate.update(
                    "UPDATE user_master2 SET lon_del_kbn = ? WHERE user_id = ?",
                    ExamJLondelConstant.LON_DEL_ON,
                    DeleteParam.getUserId()
            );
    }
}
