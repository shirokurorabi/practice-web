package com.example.examJ.repository.dao;

import com.example.examJ.repository.dto.RequestPassEditDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class EditPassParamDao {

    @Autowired
    JdbcTemplate jdbcTemplate;

    public int PassEdit(RequestPassEditDto EditParam) {
        return jdbcTemplate.update(
                "UPDATE user_master2 SET password = ? WHERE user_id = ?",
                EditParam.getPasswordNew(),
                EditParam.getUserId()
        );
    }
}
