package com.example.examJ.repository.dao;

import com.example.examJ.repository.dto.UserMasterDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;


@Repository
public class EditUserParamDao {

    @Autowired
    JdbcTemplate jdbcTemplate;

    public int Edit(UserMasterDto Param) {

        return jdbcTemplate.update(
                "UPDATE user_master2 SET user_name = ?, admin = ?, update_time = ? WHERE user_id = ?",
                Param.getUserName(),
                Param.getAdmin(),
                Param.getUpdateTime(),
                Param.getUserId()
        );
    }
}

