package com.example.examJ.repository.dao;


import com.example.examJ.constant.ExamJLondelConstant;
import com.example.examJ.repository.dto.RequestListAddParamDto;
import com.example.examJ.repository.dto.SearchParamDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;


@Repository
public class GetListParamDao {

    @Autowired
    JdbcTemplate jdbcTemplate;


    public List<Map<String, Object>> ListGet(SearchParamDto Param) throws Exception {

        List<Map<String, Object>> res = new ArrayList<>();
        try {
            String sql = "SELECT user_id, user_name, admin FROM user_master2 WHERE user_id LIKE ? AND user_name LIKE ? AND lon_del_kbn = ?";

            if (Param.getAdminList().size() != 0) {
                // adminが検索条件に指定されている場合sqlの条件を組み立てて追加する
                StringBuilder sqlIn = new StringBuilder(" AND admin IN (");
                for (String i : Param.getAdminList()) {
                    sqlIn.append(i).append(",");
                }
                sqlIn.setLength(sqlIn.length() - 1);
                sqlIn.append(")");
                sql += sqlIn;
            }


            // 検索実行
            res = jdbcTemplate.queryForList(
                    sql,
                    Param.getUserId(),
                    Param.getUserName(),
                    ExamJLondelConstant.LON_DEL_OFF
            );

        } catch(EmptyResultDataAccessException ignored) {
            // 0件時は何もしない
        }

        return res;
    }

    // メソッドのオーバーロードをしてみる（別のメソッドを定義してもよかったが・・・）
    // こちらはuser_idを複数渡してレコードを取得する処理
    public List<Map<String, Object>> ListGet(RequestListAddParamDto Param) throws Exception {
        List<Map<String, Object>> res;

        String sql = "SELECT * FROM user_master2";
        // Mapで受け取ったユーザー情報を加工
        StringBuilder sqlIn = new StringBuilder(" WHERE lon_del_kbn = '0' AND user_id IN (");
        for (Map<String, Object> i : Param.getUserList()) {
            // get()でMapのキーを指定する
            String userId = "'" + i.get("user_id") + "'";
            sqlIn.append(userId).append(",");
        }
        sqlIn.setLength(sqlIn.length() - 1);
        sqlIn.append(")");

        sql += sqlIn;


        res = jdbcTemplate.queryForList(sql);

        return res;
    }
}
