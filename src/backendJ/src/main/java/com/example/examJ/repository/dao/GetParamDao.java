package com.example.examJ.repository.dao;

import com.example.examJ.repository.dto.UserMasterDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Map;


@Repository
public class GetParamDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public UserMasterDto RecGet(String user_id) throws Exception {

        UserMasterDto result = new UserMasterDto();
        try {
            Map<String, Object> res = jdbcTemplate.queryForMap(
                    "SELECT * FROM user_master2 WHERE lon_del_kbn = '0' AND user_id = ? ", user_id
            );

            // Mapの検索結果をDTOにセットして返却
            if ( res.size() != 0) {
                result.setUserId((String) res.get("user_id"));
                result.setPassword((String) res.get("password"));
                result.setUserName((String) res.get("user_name"));
                result.setAdmin((String) res.get("admin"));
            }

        } catch(EmptyResultDataAccessException ignored) {
            // 0件時は例外が発生するが何もしない
        }


        return result;
    }
}
