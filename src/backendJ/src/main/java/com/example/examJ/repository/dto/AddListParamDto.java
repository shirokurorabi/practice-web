package com.example.examJ.repository.dto;

import java.util.List;
import java.util.Map;

public class AddListParamDto {

    private List<Map<String, Object>> user_list;

    private String register_dt;


    public List<Map<String, Object>> getUserList() {
        return this.user_list;
    }

    public void setUserList(List<Map<String, Object>> user_list) {
        this.user_list = user_list;
    }

    public String getRegisterDt() {
        return this.register_dt;
    }

    public void setRegisterDt (String register_dt) {
        this.register_dt = register_dt;
    }
}
