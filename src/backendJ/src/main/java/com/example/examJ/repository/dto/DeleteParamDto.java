package com.example.examJ.repository.dto;

public class DeleteParamDto {

    private String user_id;

    public void setUserId(String user_id) {
        this.user_id = user_id;
    }

    public String getUserId() {
        return this.user_id;
    }
}
