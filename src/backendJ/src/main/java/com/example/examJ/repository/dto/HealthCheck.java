package com.example.examJ.repository.dto;


import com.fasterxml.jackson.annotation.JsonProperty;

public class HealthCheck {

    @JsonProperty("check_id")
    private String check_id;

    @JsonProperty("check_name")
    private String check_name;

    // コンストラクタ
    public HealthCheck (String check_id, String check_name ) {
        this.check_id = check_id;
        this.check_name = check_name;
    }

//    public void setCheckId (String check_id) {
//        this.check_id = check_id;
//    }
//
//
//    public String getCheckId () {
//        return check_id;
//    }
//
//    public void setCheckName (String check_name) {
//        this.check_name = check_name;
//    }
//
//    public String getCheckName () {
//        return check_name;
//    }
//

}
