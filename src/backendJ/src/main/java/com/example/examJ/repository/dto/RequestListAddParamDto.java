package com.example.examJ.repository.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import java.util.Map;

public class RequestListAddParamDto {

    @JsonProperty("user_list")
    private List<Map<String, Object>> user_list;

    public List<Map<String, Object>> getUserList() {
        return this.user_list;
    }

}
