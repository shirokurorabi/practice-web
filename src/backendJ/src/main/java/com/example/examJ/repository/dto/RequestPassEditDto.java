package com.example.examJ.repository.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RequestPassEditDto {

    @JsonProperty("user_id")
    private String user_id;

    @JsonProperty("password_old")
    private String password_old;

    @JsonProperty("password_new")
    private String password_new;


    public String getUserId() {
        return user_id;
    }

    public String getPasswordOld() {
        return password_old;
    }

    public String getPasswordNew() {
        return password_new;
    }
}
