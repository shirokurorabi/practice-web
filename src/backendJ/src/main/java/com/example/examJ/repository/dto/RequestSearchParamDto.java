package com.example.examJ.repository.dto;


import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;


public class RequestSearchParamDto {

    @JsonProperty("user_id")
    private String user_id;

    @JsonProperty("user_name")
    private String user_name;

    @JsonProperty("admin")
    private List<String> admin;

    public void setUserId(String user_id) {
        this.user_id = user_id;
    }

    public String getUserId() {
        return user_id;
    }

    public void setUserName(String user_name) {
        this.user_name = user_name;
    }

    public String getUserName() {
        return user_name;
    }

    public void setAdminList(List<String> admin) {
        this.admin = admin;
    }

    public List<String> getAdminList() {
        return admin;
    }

}
