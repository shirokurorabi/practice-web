package com.example.examJ.repository.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RequestUserMasterDto {

    // ユーザー登録API - JSONデータを受け取りためのDTO

    @JsonProperty("user_id")
    private String user_id;

    @JsonProperty("password")
    private String password;

    @JsonProperty("user_name")
    private String user_name;

    @JsonProperty("admin")
    private String admin;

    public String getUserId() {
        return this.user_id;
    }

    public String getPassword() {
        return this.password;
    }

    public String getUserName() {
        return this.user_name;
    }

    public String getAdmin() {
        return this.admin;
    }

}
