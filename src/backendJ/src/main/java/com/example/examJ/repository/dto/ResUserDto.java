package com.example.examJ.repository.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ResUserDto {

    @JsonProperty("user_id")
    private String user_id;

    @JsonProperty("password")
    private String password;

    @JsonProperty("user_name")
    private String user_name;

    @JsonProperty("admin")
    private String admin;

    public ResUserDto (String user_id, String password, String user_name, String admin) {
        this.user_id = user_id;
        this.password = password;
        this.user_name = user_name;
        this.admin = admin;
    }

}
