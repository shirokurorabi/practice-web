package com.example.examJ.repository.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ResponseDto {

    // データ返却用DTO

    @JsonProperty("result_cd")
    private String result_cd;

    @JsonProperty("message")
    private String message;

    @JsonProperty("data")
    private ResUserDto data;


    // コンストラクタでrequestの値を設定する
    public ResponseDto(String result_cd, String message) {
        this.result_cd = result_cd;
        this.message = message;
    }

    // オーバーロードでユーザー情報をdataに代入するコンストラクタを作成する
    public ResponseDto(String result_cd, String message, ResUserDto data) {
        this.result_cd = result_cd;
        this.message = message;
        this.data = data;
    }
}
