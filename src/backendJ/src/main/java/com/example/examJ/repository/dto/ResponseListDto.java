package com.example.examJ.repository.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import java.util.Map;

public class ResponseListDto {

    @JsonProperty("result_cd")
    private String result_cd;
    @JsonProperty("message")
    private String message;
    @JsonProperty("data")
    private List<Map<String, Object>> data;

    // コンストラクタでrequestの値を設定する
    public ResponseListDto(String result_cd, String message) {
        this.result_cd = result_cd;
        this.message = message;
    }

    // オーバーロードで配列のdataに代入するコンストラクタを作成する
    public ResponseListDto(String result_cd, String message, List<Map<String, Object>> data) {
        this.result_cd = result_cd;
        this.message = message;
        this.data = data;
    }
}
