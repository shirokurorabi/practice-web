package com.example.examJ.repository.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;


public class UserMasterDto implements Serializable {

    // user_master2 DAO連携用DTO

    private static final long serialVersionUID = 1L;

    private String lon_del_kbn;

    private String user_id;

    private String password;

    private String user_name;

    private String admin;

    private String register_dt;

    private String update_time;

    public void setLonDelKbn(String lon_del_kbn) {
        this.lon_del_kbn = lon_del_kbn;
    }

    public String getLonDelKbn() {
        return lon_del_kbn;
    }

    public void setUserId(String user_id) {
        this.user_id = user_id;
    }

    public String getUserId() {
        return user_id;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public void setUserName(String user_name) {
        this.user_name = user_name;
    }

    public String getUserName() {
        return user_name;
    }

    public void setAdmin(String admin) {
        this.admin = admin;
    }

    public String getAdmin() {
        return admin;
    }

    public void setRegisterDt(String register_dt) {
        this.register_dt = register_dt;
    }

    public String getRegisterDt() {
        return register_dt;
    }

    public void setUpdateTime(String update_time) {
        this.update_time = update_time;
    }

    public String getUpdateTime() {
        return update_time;
    }
}
