package com.example.examJ.service;

import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

import com.example.examJ.repository.dao.*;
import com.example.examJ.repository.dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.examJ.constant.ExamJMsgConstant;
import com.example.examJ.constant.ExamJResultConstant;
import com.example.examJ.constant.ExamJLondelConstant;


@Service
public class UserAddService {

    // DAOは自動注入にて使用する
    @Autowired
    GetParamDao getParamDao;
    @Autowired
    AddParamDao addParamDao;

    public ResponseDto AddUser(RequestUserMasterDto RequestParam) throws Exception {
        // 現在時刻オブジェクト
        Date date = new Date();
        // 日付フォーマット設定
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");


        // ユーザー事前存在確認
        UserMasterDto GetRusult = getParamDao.RecGet(RequestParam.getUserId());

        ResponseDto result;
        if (GetRusult.getUserId() == null) {
            // ユーザーが存在しなかった場合は登録を行う

            // DAO連携用DTOに値を設定する
            UserMasterDto AddParam = new UserMasterDto();

            AddParam.setLonDelKbn(ExamJLondelConstant.LON_DEL_OFF);
            AddParam.setUserId(RequestParam.getUserId());
            AddParam.setPassword(RequestParam.getPassword());
            AddParam.setUserName(RequestParam.getUserName());
            AddParam.setAdmin(RequestParam.getAdmin());
            AddParam.setRegisterDt(formatter.format(date));

            // 登録処理実行 戻り値は登録件数
            int RegistCnt = addParamDao.Add(AddParam);

            if (RegistCnt == 0) {
                // 0件登録はシステムエラー
                result = new ResponseDto(
                        ExamJResultConstant.RESULT_CODE_ERR_SYSTEM,
                        ExamJMsgConstant.MESSAGE_ERR_SYSTEM
                );
            } else {
                // 0件でない時は正常終了
                result = new ResponseDto(
                        ExamJResultConstant.RESULT_CODE_OK,
                        ExamJMsgConstant.MESSAGE_OK_001
                );
            }
        } else {
            // ユーザーが存在した場合は登録しない
            result = new ResponseDto(
                    ExamJResultConstant.RESULT_CODE_ERR,
                    ExamJMsgConstant.MESSAGE_ERR_ID_ALREDY
            );
        }

        return result;
    }
}
