package com.example.examJ.service;


import com.example.examJ.constant.ExamJMsgConstant;
import com.example.examJ.constant.ExamJResultConstant;
import com.example.examJ.repository.dao.AddListParamDao;
import com.example.examJ.repository.dao.GetListParamDao;
import com.example.examJ.repository.dto.AddListParamDto;
import com.example.examJ.repository.dto.RequestListAddParamDto;
import com.example.examJ.repository.dto.ResponseDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Map;
import java.util.Date;
import java.text.SimpleDateFormat;


@Service
public class UserCsvAddService {

    @Autowired
    AddListParamDao addListParamDao;

    @Autowired
    GetListParamDao getListParamDao;

    public ResponseDto AddCsvUser (RequestListAddParamDto RequestListParam) throws Exception {

        // 現在日付オブジェクト
        Date date = new Date();
        // データフォーマット
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

        // 存在確認
        List<Map<String, Object>> res = getListParamDao.ListGet(RequestListParam);

        ResponseDto result;
        if (res.size() == 0) {

            AddListParamDto addListParam = new AddListParamDto();

            addListParam.setUserList(RequestListParam.getUserList());
            addListParam.setRegisterDt(formatter.format(date));


            int AddListCnt = addListParamDao.AddList(addListParam);
            if (AddListCnt == 0) {
                // 何も更新されなかった場合はシステムエラー
                result = new ResponseDto(
                        ExamJResultConstant.RESULT_CODE_ERR_SYSTEM,
                        ExamJMsgConstant.MESSAGE_ERR_SYSTEM
                );
            } else {
                // 戻り値が0件ではない = 更新された場合は正常終了
                result = new ResponseDto(
                        ExamJResultConstant.RESULT_CODE_OK,
                        ExamJMsgConstant.MESSAGE_OK_001
                );
            }
        } else {
            // CSVにすでに登録されているユーザーIDが存在する場合
            result = new ResponseDto(
                    ExamJResultConstant.RESULT_CODE_ERR,
                    ExamJMsgConstant.MESSAGE_ERR_CSV_ALREADY
                    );
        }

        return result;
    }
}
