package com.example.examJ.service;

import com.example.examJ.constant.ExamJMsgConstant;
import com.example.examJ.constant.ExamJResultConstant;
import com.example.examJ.repository.dao.DelParamDao;
import com.example.examJ.repository.dto.ResponseDto;
import com.example.examJ.repository.dto.DeleteParamDto;
import com.example.examJ.repository.dto.RequestUserMasterDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserDeleteService {

    @Autowired
    DelParamDao delParamDao;

    public ResponseDto DelUser (RequestUserMasterDto RequestParam) throws Exception {


        DeleteParamDto deleteParam = new DeleteParamDto();

        deleteParam.setUserId(RequestParam.getUserId());

        int DeleteCnt = delParamDao.DelRec(deleteParam);
        ResponseDto result;
        if (DeleteCnt == 0) {
            result = new ResponseDto(
                    ExamJResultConstant.RESULT_CODE_ERR_SYSTEM,
                    ExamJMsgConstant.MESSAGE_ERR_SYSTEM
            );
        } else {
            result = new ResponseDto(
                    ExamJResultConstant.RESULT_CODE_OK,
                    ExamJMsgConstant.MESSAGE_OK_001
            );
        }

        return result;
    }
}
