package com.example.examJ.service;


import com.example.examJ.constant.ExamJMsgConstant;
import com.example.examJ.constant.ExamJResultConstant;
import com.example.examJ.repository.dao.EditUserParamDao;
import com.example.examJ.repository.dto.RequestUserMasterDto;
import com.example.examJ.repository.dto.ResponseDto;
import com.example.examJ.repository.dto.UserMasterDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;

@Service
public class UserEditService {

    @Autowired
    EditUserParamDao editUserParamDao;

    public ResponseDto EditUser (RequestUserMasterDto RequestParam) throws Exception {
        // 現在時刻オブジェクト
        Date date = new Date();
        // 日付フォーマット設定
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        // DAO連携用DTOへ値を格納する
        UserMasterDto EditParam = new UserMasterDto();

        EditParam.setUserId(RequestParam.getUserId());
        EditParam.setUserName(RequestParam.getUserName());
        EditParam.setAdmin(RequestParam.getAdmin());
        EditParam.setUpdateTime(formatter.format(date));


        // 更新処理実行
        int EditCnt = editUserParamDao.Edit(EditParam);

        ResponseDto result;
        if (EditCnt == 0) {
            result = new ResponseDto(
                    ExamJResultConstant.RESULT_CODE_ERR_SYSTEM,
                    ExamJMsgConstant.MESSAGE_ERR_SYSTEM
            );
        } else {
            result = new ResponseDto(
                    ExamJResultConstant.RESULT_CODE_OK,
                    ExamJMsgConstant.MESSAGE_OK_001
            );
        }

        return result;
    }
}
