package com.example.examJ.service;


import com.example.examJ.constant.ExamJMsgConstant;
import com.example.examJ.constant.ExamJResultConstant;
import com.example.examJ.repository.dao.EditPassParamDao;
import com.example.examJ.repository.dao.GetParamDao;
import com.example.examJ.repository.dto.RequestPassEditDto;
import com.example.examJ.repository.dto.ResponseDto;
import com.example.examJ.repository.dto.UserMasterDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class UserPassEditService {

    @Autowired
    GetParamDao getParamDao;

    @Autowired
    EditPassParamDao editPassParamDao;

    public ResponseDto PassEdit(RequestPassEditDto RequestParam) throws Exception {

        ResponseDto result;

        UserMasterDto NowRec = getParamDao.RecGet(RequestParam.getUserId());
        if (NowRec.getPassword().equals(RequestParam.getPasswordOld())) {
            // 入力された既存のパスワードとdbのパスワードが一致した場合
            if (NowRec.getPassword().equals(RequestParam.getPasswordNew())) {
                // 古いパスワードを設定しようとしている場合
                result = new ResponseDto(
                        ExamJResultConstant.RESULT_CODE_ERR,
                        ExamJMsgConstant.MESSAGE_ERR_OLDPASS_SET
                );
            } else {
                // 既存のパスワードと被らないパスワードを入力した場合は更新する
                int EditPassCnt = editPassParamDao.PassEdit(RequestParam);
                if (EditPassCnt == 0) {
                    // 0件更新時はシステムエラー
                    result = new ResponseDto(
                            ExamJResultConstant.RESULT_CODE_ERR_SYSTEM,
                            ExamJMsgConstant.MESSAGE_ERR_SYSTEM
                    );
                } else {
                    // 正常終了
                    result = new ResponseDto(
                            ExamJResultConstant.RESULT_CODE_OK,
                            ExamJMsgConstant.MESSAGE_OK_001
                    );
                }
            }
        } else {
            // 入力された既存のパスワードとdbのパスワードが一致しなかった場合
            result = new ResponseDto(
                    ExamJResultConstant.RESULT_CODE_ERR,
                    ExamJMsgConstant.MESSAGE_ERR_OLDPASS_UNMATCH
            );
        }

        return result;
    }
}
