package com.example.examJ.service;

import com.example.examJ.constant.ExamJMsgConstant;
import com.example.examJ.constant.ExamJResultConstant;
import com.example.examJ.repository.dao.GetListParamDao;
import com.example.examJ.repository.dto.ResponseListDto;
import com.example.examJ.repository.dto.RequestSearchParamDto;
import com.example.examJ.repository.dto.SearchParamDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class UserSearchService {

    @Autowired
    GetListParamDao getListParamDao;

    public ResponseListDto SearchUser(RequestSearchParamDto RequestParam) throws Exception {

        // ワイルドカード検索ができるように加工する
        SearchParamDto SearchParam = new SearchParamDto();

        SearchParam.setUserId("%" + RequestParam.getUserId() + "%");
        SearchParam.setUserName("%" + RequestParam.getUserName() + "%");
        SearchParam.setAdminList(RequestParam.getAdminList());

        // ユーザー情報のリストを取得
        List<Map<String, Object>> ResultData = getListParamDao.ListGet(SearchParam);

        return new ResponseListDto(
                ExamJResultConstant.RESULT_CODE_OK,
                ExamJMsgConstant.MESSAGE_OK_001,
                ResultData
        );
    }
}
