package com.example.examJ.service;

import com.example.examJ.repository.dao.GetParamDao;
import com.example.examJ.repository.dto.ResponseDto;
import com.example.examJ.repository.dto.UserMasterDto;
import com.example.examJ.repository.dto.ResUserDto;
import com.example.examJ.repository.dto.RequestUserMasterDto;
import com.example.examJ.constant.ExamJResultConstant;
import com.example.examJ.constant.ExamJMsgConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class UserSignInService {

    @Autowired
    GetParamDao getParamDao;

    public ResponseDto SignIn (RequestUserMasterDto RequestParam) throws Exception {
        // ユーザーの存在確認 なければエラー
        UserMasterDto res = getParamDao.RecGet(RequestParam.getUserId());

        ResponseDto result;
        if (res.getUserId() == null) {
            result = new ResponseDto(
                    ExamJResultConstant.RESULT_CODE_ERR,
                    ExamJMsgConstant.MESSAGE_ERR_IDPASS_UNMATCH
            );
        } else {
            if (res.getPassword().equals(RequestParam.getPassword())) {
                ResUserDto userInfo = new ResUserDto(
                        res.getUserId(),
                        null,
                        res.getUserName(),
                        res.getAdmin()
                );
                result = new ResponseDto(
                        ExamJResultConstant.RESULT_CODE_OK,
                        ExamJMsgConstant.MESSAGE_OK_001,
                        userInfo
                );
            } else {
                result = new ResponseDto(
                        ExamJResultConstant.RESULT_CODE_ERR,
                        ExamJMsgConstant.MESSAGE_ERR_IDPASS_UNMATCH
                );
            }
        }

        return result;
    }
}
