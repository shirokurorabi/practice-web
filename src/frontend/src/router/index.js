import Vue from 'vue';
import Router from 'vue-router';
import HelloWorld from '../components/page/HelloWorld';
import Home from '../components/page/Home';
import Login from '../components/page/Login';
import UserAdd from "../components/page/UserAdd";
import UserEdit from "../components/page/UserEdit";
import PasswordEdit from "../components/page/PasswordEdit";
import SearchUser from "../components/page/SearchUser";
import InputCsv from "../components/page/InputCsv";


Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/hello',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/add_user',
      name: 'UserAdd',
      component: UserAdd
    },
    {
      path: '/edit_user',
      name: 'UserEdit',
      component: UserEdit
    },
    {
      path: '/edit_password',
      name: 'PasswordEdit',
      component: PasswordEdit
    },
    {
      path: '/search_user',
      name: 'SearchUser',
      component: SearchUser
    },
    {
      path: '/csv_input',
      name: 'InputCsv',
      component: InputCsv
    }
  ]
  }
)