import axios from 'axios';
axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

export default {
	post: async function (originUrl, params) {
		// console.log(process.env.VUE_APP_API_URL);
		// let reqUrl = process.env.VUE_APP_API_URL +  originUrl;
		let headers = {
			'Content-Type': 'application/json;charset=UTF-8',
			'Access-Control-Allow-Origin': '*',
			'Access-Control-Allow-Headers': '*'
		};
		// pythonサーバーの場合はこちら
		//let reqUrl = 'http://0.0.0.0:3010' + originUrl;
		
		// springサーバーの場合はこちら
		let reqUrl = 'http://0.0.0.0:3011' + originUrl;
		
		return await axios.post(reqUrl, params, {headers: headers})
	}
}