import Vue from 'vue';
import Vuex from 'vuex';
import auth from './model/auth';
import user from './model/user';


Vue.use(Vuex);

export default new Vuex.Store({
	modules: {
		auth,
		user
	},
	strict: true
});