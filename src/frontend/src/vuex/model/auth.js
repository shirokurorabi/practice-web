import api from '../api/api';
const md5 = require('js-md5');
const encrypt = (user_id, password) => md5(user_id + password);

export default {
	namespaced: true,
	state: {
		login_info_param: {
			user_id: '',
			password: '',
			user_name: '',
			admin: ''
		},
		change_pass_param: {
			user_id: '',
			password_old: '',
			password_new: ''
		},
		AuthLoginFlg: false,
		LoginMsg: {},
		LoginOverRayFlg: false
	},
	mutations: {
		setLoginInfo: function (state, data=null) {
			// ログイン用情報設定
			for (let k in data) {
				if ( k === 'password') {
					state.login_info_param['password'] = encrypt(data['user_id'].data, data['password'].data);
				} else {
					state.login_info_param[k] = data[k].data;
				}
			}
		},
		setLoginMsg: function (state, data=null) {
			state.LoginMsg = data;
		},
		setLoginFlg: function (state, data=null) {
			state.AuthLoginFlg = data;
		},
		setLoginOverRayFlg: function (state, data=false) {
			state.LoginOverRayFlg = data
		},
		setLoginUserInfo: function (state, data=null) {
			// 今ログインしているユーザー情報を設定
			for ( let k in data) {
				if (k === 'password') {
					state.login_info_param[k] = '';
				} else {
					state.login_info_param[k] = data[k];
				}
			}
		},
		setChangeLoginInfo: function (state, data=null) {
			for ( let k in data ) {
				state.login_info_param[k] = data[k].data;
			}
		},
		setChangePassWord: function (state, data=null) {
			for (let k in state.change_pass_param) {
				if (k === 'user_id') {
					state.change_pass_param[k] = state.login_info_param[k];
				} else {
					state.change_pass_param[k] = encrypt(state.login_info_param['user_id'], data[k].data);
				}
			}
		},
		offLoginUserInfo: function (state) {
			for (let k in state.login_info_param) {
				state.login_info_param[k] = '';
			}
		}
	},
	actions: {
		async userLogin ({ commit, state }) {
			let result = await api.post('/api/sign_in', state.login_info_param);
			console.log(result);
			if (result.data.result_cd === '001') {
				commit('setLoginUserInfo', result.data.data);
				commit('setLoginFlg', true);
				commit('setLoginOverRayFlg', false);
			} else {
				commit('setLoginMsg', result.data.message);
			}
		},
		async userLogout({ commit }) {
			commit('setLoginMsg', '');
			commit('offLoginUserInfo');
			commit('setLoginFlg', false);
		},
		async passwordUpdate ({ state }) {
			return await api.post('/api/user/password_update', state.change_pass_param);
		},
		setLoginParam: function({commit}, data) {
			commit('setLoginInfo', data);
		},
		setLoginMessage: function({commit}, data) {
			commit('setLoginMsg', data);
		},
		setPassWord: function({commit}, data) {
			commit('setChangePassWord', data);
		},
		onLoginFlg: function ({commit}) {
			commit('setLoginFlg', true);
		},
		offLoginFlg: function ({commit}) {
			commit('setLoginFlg', false);
		},
		onOverRayFlg: function ({commit}) {
			commit('setLoginOverRayFlg', true);
		},
		offOverRayFlg: function ({commit}) {
			commit('setLoginOverRayFlg', false);
		},
		changeLoginInfo: function ({commit}, data) {
			commit('setChangeLoginInfo', data);
		}
	}
}