import api from '../api/api';
const md5 = require('js-md5');
const encrypt = (user_id, password) => md5(user_id + password);

export default {
	namespaced: true,
	state: {
		user_info_param: {
			user_id: '',
			user_name: '',
			password: '',
			admin: ''
		},
		user_search_param: {
			user_id: '',
			user_name: '',
			admin: []
		},
		user_delete_param: {
			user_id: ''
		},
		add_users: {
			user_list: []
		},
		UserErrMsg: {}
	},
	mutations: {
		setUserInfo: function (state, data=null) {
			for (let k in data) {
				if ( k === 'password1' || k === 'password2'  ) {
					state.user_info_param['password'] = encrypt(data['user_id'].data, data['password1'].data);
				} else {
					state.user_info_param[k] = data[k].data;
				}
			}
		},
		setUserSearch: function (state, data=null) {
			for (let k in state.user_search_param) {
				state.user_search_param[k] = data[k].data;
			}
		},
		setUserDelete: function(state, data=null) {
			state.user_delete_param.user_id = data;
		},
		setUserList: function(state, data=null) {
			state.add_users.user_list = [];
			console.log(data);
			for (let col in data) {
				let tmp = {
					'user_id': '',
					'password': '',
					'user_name': '',
					'admin': '',
				};
				for (let rec in data[col]) {
					switch (rec) {
						case 'password':
							tmp['password'] = encrypt(data[col]['user_id'], data[col]['password']);
							break;
							
						case 'admin':
							switch (data[col]['admin']) {
								case 'A':
									tmp['admin'] = '0';
									break;
								case 'B':
									tmp['admin'] = '1';
									break;
								case 'C':
									tmp['admin'] = '2';
									break;
								case 'D':
									tmp['admin'] = '3';
									break;
							}
							break;
							
						default:
							tmp[rec] = data[col][rec];
							break;
					}
				}
				state.add_users.user_list.push(tmp);
			}
		}
	},
	actions: {
		async userAdd ({ state }) {
			console.log(state.user_info_param);
			return await api.post('/api/user/user_add', state.user_info_param);
		},
		async userUpdate ({ state }) {
			return await api.post('/api/user/user_update', state.user_info_param);
		},
		async userSearch ({ state }) {
			return await api.post('/api/user/user_search', state.user_search_param);
		},
		async userDelete ({ state }) {
			return await api.post('/api/user/user_delete', state.user_delete_param);
		},
		async userListAdd ({state}) {
			console.log(state.add_users);
			return await api.post('/api/user/user_csv_add', state.add_users);
		},
		setUserAddParam: function({commit}, data) {
			commit('setUserInfo', data);
		},
		setUserSearchParam: function({commit}, data) {
			commit('setUserSearch', data);
		},
		setUserDeleteParam: function({commit}, data) {
			commit('setUserDelete', data)
		},
		serUserListParam: function({commit}, data) {
			commit('setUserList', data)
		},
		healthCheck: async function () {
			// let paramA = { param: "health check OK"};
			let res = await api.post('/health/check');
			console.log(res);
		}
	}
}